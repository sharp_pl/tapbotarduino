//include pin definitions and setup.
#include "pins.h"

//including Tap.cpp for Tap object:
#include "Tap.cpp"

//including MemoryFree.h for memory managment of the buffer.
#include "MemoryFree.h"

//including Servo.h for the servo on the tapbot.
#include <Servo.h>

//servo object:
Servo myservo;

//max values, these will be changed when 'cal;' is called (Calibration)
int maxX(1200), maxY(1500);

//some defaults
int dirX(HIGH), dirY(HIGH);     //default directions.
int posX(1), posY(1);           //position of tapbot when starting
boolean tapDown = false;        //keep track of stylus status.
int servoPos = startPos;        //servo automatically goes to it's zero point, but it should go to about half.

boolean debug = false;
int debug_counter = 0;

//buffer to store tap commands in
Tap taps[BUFFER_SIZE];
int index = 0;

boolean full = false;
boolean setup_servo = false;

void loop() {
  init_servo();
  if (Serial.available()) {
    String incoming = Serial.readStringUntil(';');   //incoming serial string until ';'


    if (incoming.startsWith("repeat")) {            //repeating buffer until serial data is available again.
      while (!Serial.available()) {
        handleInstruction(incoming, false);
      }

    } else if (incoming.startsWith("tapt")) {       //tap toggle without buffer
      Serial.println("> " + incoming);
      taptoggle();

    } else if (incoming.startsWith("tap")) {        //tap without buffer
      Serial.println("> " + incoming);
      tap_();

    } else if (incoming.startsWith("tt")) {         //put tap toggle in buffer
      incoming = "g0,0,2";
      handleInstruction(incoming, true);

    } else if (incoming.startsWith("dy")) {         //put delay in buffer
      incoming = "g" + getValue(incoming.substring(2), SEPERATOR, 0) + ",0,3";
      handleInstruction(incoming, true);

    } else if (incoming.startsWith("t")) {          //put tap in buffer
      incoming = "g0,0,1";
      handleInstruction(incoming, true);

    } else if (incoming.startsWith("g")) {          //put coordinates in buffer
      handleInstruction(incoming, true);

    } else if (incoming.startsWith("m")) {          //read and execute buffer
      handleInstruction(incoming, false);

    } else if (incoming.startsWith("h")) {          //go to home position (1,1)
      Serial.println("> Going home...");
      goHome();

    } else if (incoming.startsWith("p")) {          //returns position
      Serial.println("> Position at " + String(posX) + "," + String(posY));

    } else if (incoming.startsWith("reset")) {      //resets coordinates.
      Serial.println("> Reset (1,1)");
      posX = 1;
      posY = 1;

    } else if (incoming.startsWith("c")) {          //starts calibration sequence.
      Serial.println("> Calibrating");
      calibrate();
      Serial.println("> Found maxX=" + String(maxX) + " maxY=" + String(maxY) + "");

    } else if (incoming.startsWith("debug")) {      //enables debug function to print data to serial.
      if (debug)
        debug = false;
      else
        debug = true;

    } else if (incoming.startsWith("i")) {          //print buffer info
      Serial.println("> Info");
      Serial.print("> Mem: ");
      Serial.println(freeMemory());
      Serial.println("> Index: " + String(index));
      if (!full) return;

    } else if (incoming.startsWith("up")) {         //manual command to go up X steps without buffer
      Serial.println("> " + incoming);
      incoming = incoming.substring(2);
      goToPos(posX + incoming.toInt(), posY);

    } else if (incoming.startsWith("down")) {       //manual command to go down X steps without buffer
      Serial.println("> " + incoming);
      incoming = incoming.substring(4);
      goToPos(posX - incoming.toInt(), posY);

    } else if (incoming.startsWith("left")) {       //manual command to go left X steps without buffer
      Serial.println("> " + incoming);
      incoming = incoming.substring(4);
      goToPos(posX , posY - incoming.toInt());

    } else if (incoming.startsWith("right")) {      //manual command to go right X steps without buffer
      Serial.println("> " + incoming);
      incoming = incoming.substring(5);
      goToPos(posX , posY + incoming.toInt());

    } else if (incoming.startsWith("save")) {       //save position in buffer, comes in handy when programming the tapbot manually.
      String i = "g" + String(posX) + "," + String(posY);
      handleInstruction(i, true);

    } else if (incoming.startsWith("drop")) {       //prints out the buffer in a format that can be send to tapbot again.
      for (int i = 0; i < index; i++) {
        Tap tap = taps[i];
        Serial.print("g" + String(tap.x) + "," + String(tap.y) + "," + String(tap.z) + ";");
      }

    } else if (incoming.startsWith("flush")) {      //empty the buffer
      index = 0;

    } else {
      Serial.println("> Unknown cmd: " + incoming); //unknown command
    }

  }
  debug_(); //debug each cycle.
}

//handle instructions and puts it in buffer if needed.
void handleInstruction(String &incoming, boolean inputBuffer) {

  if (incoming.startsWith("g")){
    //put data in buffer, z indicates what kind of instruction it is. The reason it's called 'z' is because it has to do with the z-axis most of the time.
    Serial.println("> Put '" + incoming + "' in buffer");
    int x = getVal(incoming, (0));
    int y = getVal(incoming, (1));
    int z = getVal(incoming, (2));
    if ((index + 1) == BUFFER_SIZE) return;
    taps[index] = Tap(x, y, z);
    index++;
    full = true;

    //if it shouldn't be put in to buffer, the buffer should be read and executed!
  } else if (!inputBuffer) {

    //for each item in the buffer
    for (int i = 0; i < index; i++) {
      Tap tap = taps[i];

      //if z > 0 it means that the command isn't a 'goto' but has something to do with the z-axis.
      if (tap.z > 0) {
        switch (tap.z) {
          case 1:
            Serial.println("> tap()");
            // tap once if z == 1;
            tap_();
            break;
          case 2:
            Serial.println("> taptoggle()");
            // toggle the z-axis if z == 2;
            taptoggle();
            break;
          case 3:
            // wait some time before resuming if z == 3; The parameter Tap.x carries the data for how long it should wait.
            delay(tap.x);
            break;
        }
      } else {
        //if z == 0, or if it has nothing to do with the z-axis, it executes a goToPos.

        //checking max and min values.
        if (tap.x > maxX) tap.x = maxX;
        if (tap.x < 1) tap.x = 1;
        if (tap.y > maxY) tap.y = maxY;
        if (tap.y < 1) tap.y = 1;
        goToPos(tap.x, tap.y);
      }

      
    }
  }
}


//get int value from string.
int getVal(String &s, int index) {
  return getValue(s.substring(1), SEPERATOR, index).toInt();
}


//goes to home.
void goHome() {

  //sets direction to down, and writes down to the motor driver.
  dirX = DOWN;
  digitalWrite(dirPinX, dirX);

  //it then steps until the endswitch is hit.
  while (!endSwitchX1()) {
    stepX();
  }

  //same story for the other axis.
  dirY = LEFT;
  digitalWrite(dirPinY, dirY);
  while (!endSwitchY1()) {
    stepY();
  }

  //at the end we're sure that the carriage is at 1,1
  posX  = 1;
  posY = 1;
}

//calibrates the device.
void calibrate() {
  
  //calibration first goes to home, then goes to the max values of the tapbot, then it goes back to home again for the convenience.
  goHome();

  //calibrate X
  dirX = UP;
  digitalWrite(dirPinX, dirX);
  int x;
  for (x = 0; x < 10000 && !endSwitchX2(); x++) {
    stepX();
  }
  maxX = x;
  
  //calibrate Y
  dirY = RIGHT;
  digitalWrite(dirPinY, dirY);
  int y;
  for (y = 0; y < 10000 && !endSwitchY2(); y++) {
    stepY();
  }
  maxY = y;

  //go home again.
  goHome();
}

//go to position. Positions are absolute (So they're not relative to the current location).
void goToPos(int x, int y) {
  Serial.println("> Going to pos " + String(x) + "," + String(y) + " currently at " + String(posX) + "," + String(posY));
  
  //if the position mentioned is smaller than the current position, then go back. If it's bigger go further away. If it's equal (no need to be checked) don't do anything.
  if (x < posX) {
    dirX = DOWN;
    rotateX(posX - x);
  } else if (x > posX) {
    dirX = UP;
    rotateX(x - posX);
  }

  //same goes for the other axis.
  if (y < posY) {
    dirY = LEFT;
    rotateY(posY - y);
  } else if (y > posY) {
    dirY = RIGHT;
    rotateY(y - posY);
  }
}


//4 endswitches, made functions for easier access. The reason they return !nversed values is because they're digital pullup to ground, so no need for extra resistors because it uses Arduino's internal pullup hack.
boolean endSwitchX1() {
  return !digitalRead(endX1);
}

boolean endSwitchX2() {
  return !digitalRead(endX2);
}

boolean endSwitchY1() {
  return !digitalRead(endY1);
}

boolean endSwitchY2() {
  return !digitalRead(endY2);
}


//steps X one step. Be careful, no safety features here.
void stepX() {
  digitalWrite(stepPinX, HIGH);
  delayMicroseconds(delay1);
  digitalWrite(stepPinX, LOW);
  delayMicroseconds(delay1);
}

//rotate/move X a certain amount of steps.
void rotateX(int steps) {
  digitalWrite(dirPinX, dirX);
  // Enables the motor to move in a particular direction
  // Makes 200 pulses for making one full cycle rotation
  for (int x = 0; x < steps; x++) {
    stepX();
    //change position each step.
    if (dirX == UP) {
      posX++;
    } else {
      posX--;
    }
    //check endswitches
    if ((endSwitchX1() && (posX < 10 && dirX == DOWN )) || posX < 1) {
      posX = 1;
      break;
    }

  }
}

//steps Y one step. Be careful, no safety features here.
void stepY() {
  digitalWrite(stepPinY, HIGH);
  delayMicroseconds(delay1);
  digitalWrite(stepPinY, LOW);
  delayMicroseconds(delay1);
}

//rotate/move Y a certain amount of steps.
void rotateY(int steps) {
  digitalWrite(dirPinY, dirY);
  // Enables the motor to move in a particular direction
  // Makes 200 pulses for making one full cycle rotation
  for (int Y = 0; Y < steps; Y++) {
    stepY();
    //change position each step.
    if (dirY == RIGHT) {
      posY++;
    } else {
      posY--;
    }
    //check endswitches
    if ((endSwitchY1() && (posY < 10 && dirY == LEFT )) || posY < 1) {
      posY = 1;
      break;
    }
  }
}



//tap servo controls:

//tap once
void tap_() {
  if (!tapDown) {
    tapdown();
  }
  delay(100);
  tapup();
  delay(20);
}

//toggle the tap device.
void taptoggle() {
  if (tapDown) {
    tapup();
  } else {
    tapdown();
  }
}

//moves the tapper down.
void tapdown() {
  for (; servoPos >= startPos; servoPos -= 1) {
    myservo.write(servoPos);
    delay(servoDelay);
  }
  tapDown = true;
}

//moves the tapper up
void tapup() {
  for (; servoPos <= tapReach; servoPos += 1) {
    myservo.write(servoPos);
    delay(servoDelay);
  }
  tapDown = false;
}

//initializes servomotor. Only to be executed once.
void init_servo() {
  if (setup_servo) return;
  myservo.attach(servo_pin);
  delay(servoDelay);
  for (servoPos = startPos; servoPos <= tapReach; servoPos++) {
    myservo.write(servoPos);
    delay(servoDelay);
  }

  setup_servo = true;
}

//debug function prints the status of the endswitches each second, but could be extended to print more to serial...
void debug_() {
  if (debug && (millis() - debug_counter) > 1000) {
    Serial.println("switchX1: " + String(endSwitchX1()));
    Serial.println("switchX2: " + String(endSwitchX2()));
    Serial.println("switchY1: " + String(endSwitchY1()));
    Serial.println("switchY2: " + String(endSwitchY2()));
    Serial.println();
    debug_counter = millis();
  }
}

//good luck!

