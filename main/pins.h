#ifndef PINS_H
#define PINS_H

//make it easier to read, or harder, if you keep tapbot upside down or sideways
#define UP LOW
#define DOWN HIGH
#define LEFT HIGH
#define RIGHT LOW

/** PINS **/
#define stepPinX 8
#define dirPinX 7
#define stepPinY 6
#define dirPinY 5

#define servo_pin 9

/** SETTINGS **/
#define delay1 1000
#define servoDelay 5
#define startPos 90
#define tapReach 20 + startPos

/** LIMITS **/
#define endX1 A2
#define endX2 A3
#define endY1 A1
#define endY2 A0

//buffer size, this is how many commands the tapbot can hold before executing. Could be increased if you have an Arduino MEGA, but it was not needed in this case.
#define BUFFER_SIZE 100

//character that seperates variables in a string command.
#define SEPERATOR ','

#endif


void setup() {
  // enable serial communication
  Serial.begin(9600);

  //set switches as input
  pinMode(endX1, INPUT);
  pinMode(endX2, INPUT);
  pinMode(endY1, INPUT);
  pinMode(endY2, INPUT);

  // set step and dir pin to output
  pinMode(stepPinX, OUTPUT);
  pinMode(dirPinX, OUTPUT);
  pinMode(stepPinY, OUTPUT);
  pinMode(dirPinY, OUTPUT);

  // enable pullup for switches.
  digitalWrite(endX1, HIGH);
  digitalWrite(endY1, HIGH);
  digitalWrite(endX2, HIGH);
  digitalWrite(endY2, HIGH);

  //serial flush for safety and stability
  Serial.flush();
}

//alias for free(*)
void operator delete(void* ptr)
{
  free(ptr);
}

//frees arrays.
void operator delete[](void* ptr)
{
  return (operator delete)(ptr);
}

//get value from strings.
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
