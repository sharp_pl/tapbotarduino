# README #
### This repository is for Tapbot Arduino code ###

The main folder contains all the files. The file 'main.ino' contains all the logic needed to make the tapbot work. We define our pins, setup, and some basic functions in the file 'pins.h'. Everything else doesn't need any explanation as they have to do with some basic memory management in C++.